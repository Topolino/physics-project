# Name: Joey Carberry
# Date: December 22, 2015
# Project: Program for Physics

from tkinter import *
import math
class createField:
    def newList(self):

        startList = [-138.29, -132.57, -125.63, -117.45, -108.28, -98.89, -90.43, -84.15, -81.01, -81.36, -84.52, -88.46,
                     -90.64, -89.45,-84.74, -77.41, -68.80, -60.18, -52.38, -45.74, -40.26, -143.67, -137.83, -130.30,
                     -120.75, -109.28, -96.87, -85.50, -77.33, -73.94, -76.13, -83.29, -91.96, -97.26, -96.64, -90.34,
                     -80.12, -68.42, -57.39, -48.15, -40.85, -35.20, -150.38, -144.87, -137.18, -126.39, -111.84, -94.60,
                     -78.41, -67.33, -63.46, -68.05, -81.43, -98.01, -107.86, -107.59, -98.49, -83.32, -66.59, -52.33,
                     -41.74, -34.22, -28.84, -158.55, -154.10, -147.32, -136.35, -118.29, -92.39, -67.49, -52.73, -48.75,
                     -56.08, -78.41, -107.96, -123.64, -86.51, -60.89, -42.95, -32.08, -25.39, -21.04, -168.01, -165.57,
                     -123.68, -110.69, -161.50, -153.68, -135.47, -90.69, -46.75, -31.30, -29.76, -39.99, -73.01,
                     -122.99, -144.60, -146.68, -132.52, -89.00, -44.27, -26.09, -18.33, -14.32, -11.93, -178.23,
                     -178.50, -178.86, -179.31, -179.76, 'POINT ONE', -0.44, -2.50, -8.13, -22.04, -63.43,
                     -141.85, -168.57, -176.76, -179.44, 'POINT TWO', -0.30, -0.85, -1.38, -1.80, -2.09, 171.68,
                     168.77, 164.04, 155.40, 136.33, 90.89, 45.91, 26.46, 12.86, -9.46, -56.56, -152.32, 167.75, 152.84,
                     133.56, 88.74, 43.20, 23.98, 15.25, 10.50, 7.61, 162.61, 157.86, 150.65, 139.14, 120.50, 94.11,
                     68.51, 50.95, 33.20, -35.26, -73.34, -87.22, 139.38, 124.82, 109.16, 84.31, 58.13, 39.54, 28.07,
                     20.93, 16.28, 155.05, 149.51, 141.79, 131.03, 116.75, 100.40, 86.58, 82.37, 133.06, -116.42, -86.03,
                     -54.68, 8.24, 83.71, 87.46, 75.97, 60.55, 46.73, 36.27, 28.77, 23.42, 149.07, 143.53, 136.49,
                     127.79, 117.97, 109.10, 106.39, 123.87, 177.71, -140.02, -89.41, -38.98, -2.22, 42.59, 64.73, 65.29,
                     58.03, 49.09, 40.93, 34.26, 29.02, 144.41, 139.33, 133.38, 126.83, 120.60, 116.88, 120.17, 136.87,
                     161.41, 176.02, 'POINT THREE', 3.68, 15.83, 35.83, 51.94, 56.94, 54.69, 49.44, 43.55, 38.08,
                     33.36, 140.74, 136.23, 131.33, 126.42, 122.38, 120.76, 123.65, 131.52, 138.06, 129.98, 89.72, 48.98,
                     39.28, 43.52, 50.30, 53.61, 52.86, 49.62, 45.34, 40.93, 36.80, 137.70, 133.72, 129.60, 125.69,
                     122.58, 121.02, 121.49, 122.85, 121.08, 110.42, 89.25, 67.74, 56.09, 53.07, 53.67, 54.11, 52.98,
                     50.40, 47.00, 43.32, 39.70, 135.07, 131.48, 127.86, 124.43, 121.51, 119.34, 117.76, 115.71, 111.14,
                     102.06, 88.89, 75.52, 65.87, 60.62, 58.10, 56.41, 54.43, 51.85, 48.81, 45.54, 42.28, 132.68, 129.37,
                     126.03, 122.79, 119.78, 117.00, 114.19, 110.66, 105.46, 97.97, 88.68, 79.27, 71.47, 65.90, 62.10,
                     59.20, 56.52, 53.73, 50.76, 47.71, 44.67, 130.46, 127.33, 124.14, 120.97, 117.83, 114.66, 111.24,
                     107.19, 102.09, 95.77, 88.58, 81.33, 74.84, 69.54, 65.34, 61.87, 58.76, 55.77, 52.79, 49.83, 46.93]
        self.newList = []
        print(startList[162])
        print(len(startList))
        for x in range(0,len(startList)):
            if x == 110 or x == 120 or x == 220:
                x += 1
                self.newList.append('PLACE HOLDER')
            if startList[x] < 0:
                newValue = 360 + startList[x]
                self.newList.append(newValue)
            else:
                self.newList.append(startList[x])
        print(self.newList)
        self.drawTheGrid()

    def drawTheGrid(self):
        x1 = 20
        y1 = 20
        # Creates lines that are 10px away from the other horizontally
        canvasWidthGrid = 800 / 5 + 20
        canvasHeightGrid = 600 / 5 + 20
        # Vertical Lines
        for x in range(0, int(canvasHeightGrid)):
            if x != 0:
                self.canvas.create_line(x1, 0, x1, 640, tags = 'line')
                x1 += 20
            else:
                self.canvas.create_line(x1, 0, x1, 640, tags = 'line', fill = 'red', width = 2)
        # Horizontal Lines
        # Creates lines that are 10px away from the other vertically
        for y in range(0, int(canvasWidthGrid)):
            if y != 30:
                self.canvas.create_line(0, y1, 840, y1, tags = 'line')
                y1 += 20
            else:
                self.canvas.create_line(0, y1, 840, y1, tags = 'line', fill = 'red', width = 2)
        self.drawLabels()

    def drawLabels(self):
        # Y Axis labels
        self.canvas.create_text(10, 20, text = '100')
        self.canvas.create_text(10, 220, text = '50')
        self.canvas.create_text(10, 420, text = '0')
        self.canvas.create_text(10, 630, text = '-50')
        # X Axis labels
        self.canvas.create_text(220, 630, text = '0')
        self.canvas.create_text(420, 630, text = '50')
        self.canvas.create_text(620, 630, text = '100')
        self.canvas.create_text(820, 630, text = '150')
        self.drawCharges()

    def drawCharges(self):
        # 220, 420 Charge 1 - 5e-7
        self.canvas.create_oval(224, 416, 216, 424, fill = 'blue')
        self.canvas.create_text(220, 390, text = 'Charge 1')
        self.canvas.create_text(220, 405, text = '+5e-7')
        # 420, 220 Charge 2 - 4e-7
        self.canvas.create_oval(424, 216, 416, 224, fill = 'blue')
        self.canvas.create_text(420, 190, text = 'Charge 2')
        self.canvas.create_text(410, 205, text = '+4e-7')
        # 620, 420 Charge 3 - 2e-7
        self.canvas.create_oval(624, 416, 616, 424, fill = 'blue')
        self.canvas.create_text(620, 390, text = 'Charge 3')
        self.canvas.create_text(620, 405, text = '+2e-7')
        self.drawVectors()


    def drawVectors(self):
        cordX = 20
        cordY = 620
        count = 0


        for x in range(0, len(self.newList)):
            if x == 162:
                print('TEST 162',self.newList[162])
            print()
            print(count)
            count += 1
            run = True
            if x == 121 or x == 110 or x == 222:
                x += 1
                cordX += 40
            print('Angle: ', self.newList[x])
            q = 1

            value = self.newList[x]
            while run:
                if value > 90:
                    value -= 90
                    q += 1
                else:
                    run = False
                    print('Quadrant: ', q)
                    print('Cords:', cordX, cordY)
                    startX = (math.cos(math.radians(value))) * 15
                    print('startX:', startX)
                    startY = (math.sin(math.radians(value))) * 15
                    print('startY: ', startY)
                    print('Value: ', value)
                    if q == 1:
                        self.canvas.create_line((cordX + abs(startX)), (cordY - abs(startY)), (cordX - abs(startX)),
                                                (cordY + abs(startY)), fill = 'black', width = 2, arrow = 'first')
                    if q == 2:
                        self.canvas.create_line((cordX - abs(startX)), (cordY - abs(startY)), (cordX + abs(startX)),
                                                (cordY + abs(startY)), fill = 'black', width = 2, arrow = 'first')
                    if q == 3:
                        self.canvas.create_line((cordX - abs(startX)), (cordY + abs(startY)), (cordX + abs(startX)),
                                                (cordY - abs(startY)), fill = 'black', width = 2, arrow = 'first')
                    if q == 4:
                        self.canvas.create_line((cordX + abs(startY)), (cordY + abs(startX)), (cordX - abs(startY)),
                                                (cordY - abs(startX)), fill = 'black', width = 2, arrow = 'first')
                    # Handles Coordinates
                    cordX += 40
                    if cordX >= 860:
                        cordX = 20
                        cordY -= 40

    def __init__(self):
        window = Tk()
        window.title('Physics Electric Field')

        self.canvas = Canvas(window, width = 840, height = 640, bg = 'white')
        self.canvas.pack()

        self.newList()
        window.mainloop()

createField()
